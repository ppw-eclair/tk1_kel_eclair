from django import forms
from . import models

class BuatIkan(forms.ModelForm):
    class Meta:
        model = models.Ikan
        fields = ['jenis', 'foto', 'harga', 'deskripsi', 'namaToko']