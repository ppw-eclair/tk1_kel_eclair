from django.shortcuts import render, redirect
from . import forms
from .models import Ikan

def buat_Ikan(request):
    if request.method == 'POST':
        form = forms.BuatIkan(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect ('beli_ikan:form_ikan')
    else:
        form = forms.BuatIkan()
        return render(request, 'buat_ikan.html', {'form_ikan':form})

def daftar_Ikan(request):
    if request.method=='POST':
        form = forms.BuatIkan(request.POST)
        if form.is_valid():
            form.save()
        
    semua_ikan = Ikan.objects.all().order_by('jenis','harga')
    return render(request, "daftar_ikan.html", {'semua_ikan':semua_ikan})

def hapus_Ikan(request, id):
    ikan_hapus = Ikan.objects.get(id=id)
    ikan_hapus.delete()
    return redirect('beli_ikan:form_ikan')