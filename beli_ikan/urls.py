from django.urls import path
from . import views

app_name = "beli_ikan"
urlpatterns = [
    path('', views.daftar_Ikan, name="form_ikan"),
    path('terima', views.buat_Ikan, name="terima"),
    path('delete/<id>', views.hapus_Ikan, name="hapus"),
]