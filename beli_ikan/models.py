from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from toko.models import Toko

class Ikan(models.Model):
    jenis = models.CharField(max_length = 100)
    foto = models.ImageField(upload_to='images/')
    harga = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(9223372036854775807)])
    deskripsi = models.TextField(max_length = 100)
    namaToko = models.ManyToManyField(Toko)

    def __str__(self):
        return self.jenis