from django.shortcuts import render, redirect
from .forms import question
from .models import Pertanyaan
# Create your views here.
def faq(request):
	return render(request, "beranda.html")

# def pertanyaan(request):
# 	return render(request, 'faq.html')

def pertanyaan(request):
    if request.method == 'POST':
        form = question(request.POST)
        print(form.errors)
        if form.is_valid():
            form.save()
            return redirect ('faq:faq')
    else:
        form = question()
        semua_pertanyaan = Pertanyaan.objects.all()
        return render(request, 'faq.html', {'form':form, 'semua_pertanyaan':semua_pertanyaan})
		

