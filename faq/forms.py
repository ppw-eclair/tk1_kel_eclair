from django import forms
from .models import Pertanyaan


class question(forms.ModelForm):
	class Meta:
		model = Pertanyaan
		fields = [
			'pertanyaan'
		]