from django.test import TestCase, Client 
from .models import Pertanyaan
from .forms import question

# Create your tests here.

class FAQ(TestCase):
	def test_url_faq(self):
		c = Client()
		response = c.get('/faq')
		self.assertEqual(response.status_code, 301)

	# def test_form_faq(self):
	# 	c = Client()
	# 	response = c.get('/faq')
	# 	self.assertContains(response, '<form')

	# def test_template(self):
	# 	response = self.client.get('/faq')
	# 	self.assertTemplateUsed(response, 'faq.html')


	def test_view_post(self):
		data = { 'tanya' : 'ini pertanyaan  lohhh' }
		c = Client()
		response = c.post('/faq', data)
		self.assertEqual(response.status_code, 301)

	def test_database(self):
		pertanyaan = Pertanyaan(pertanyaan = 'ini untuk coba database bagian pertanyaan')
		pertanyaan.save()
		hitung_jumlah = Pertanyaan.objects.all().count()
		self.assertEqual(hitung_jumlah, 1)

	#def test_masukin_pertanyaan(self):
		#belom
