from django import forms
from . import models

class BuatToko(forms.Form):
    foto=forms.ImageField(required=True)
    nama=forms.CharField(label='nama', 
                            max_length=100, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'Toko Sumber Ikan', 'class':'form-control'})
                            )

    nomor=forms.CharField(label='nomor', 
                            required=True, 
                            widget=forms.NumberInput(attrs={'placeholder': '087812344432', 'class':'form-control'})
                            )

    email=forms.CharField(label='email', 
                            max_length=100, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'sumberikan@gmail.com', 'class':'form-control'})
                            )

    username=forms.CharField(label='username', 
                            max_length=100,
                            min_length=8, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'sumber_ikan', 'class':'form-control'})
                            )

    password=forms.CharField(label='password', 
                            max_length=30,
                            min_length=5, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'password', 'class':'form-control'})
                            )
