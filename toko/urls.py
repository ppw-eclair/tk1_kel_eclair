from django.urls import path
from . import views

urlpatterns = [
    path('', views.toko, name="toko"),
    path('tambah/', views.tambahToko, name="tambah Toko"),
    path('tambah/buat/',  views.buat, name="buat"),
    path('hapus/', views.hapusTok, name="hapus"),
]