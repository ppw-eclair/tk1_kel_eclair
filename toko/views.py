from django.shortcuts import render,redirect
from .models import Toko
from .forms import BuatToko

# Create your views here.
def toko(request):
    # print(Toko.length())
    daftarToko=Toko.objects.order_by()
    return render(request, "toko.html", {'daftarToko':daftarToko})

def tambahToko(request):
    form=BuatToko()
    return render(request, "tambahToko.html", {'form':form})

def buat(request):
    form=BuatToko(request.POST, request.FILES)
    if(request.method=='POST' and form.is_valid()):
        toko=Toko(
                foto=request.FILES["foto"],
                nama=form.data["nama"],
                nomor=form.data["nomor"],
                email=form.data["email"],
                username=form.data["username"],
                password=form.data["password"],
            )
        toko.save()
    return redirect('/toko/')

def hapusTok(request):
    if request.method == "POST" and 'id' in request.POST :
        id = request.POST['id']
        Toko.objects.get(id=id).delete()
    return redirect('/toko/')