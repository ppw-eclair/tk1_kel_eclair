from django.test import TestCase
from django.test import Client
from django.urls import resolve

from .views import toko, tambahToko, buat, hapusTok
from .models import Toko
from .forms import BuatToko
from django.core.files.uploadedfile import SimpleUploadedFile

class TokoTest(TestCase):
    def test_toko_url_is_exist(self):
        response = Client().get('/toko/')
        self.assertEqual(response.status_code,200)

    def test_buat_toko_url_is_exist(self):
        response = Client().get('/toko/tambah/')
        self.assertEqual(response.status_code,200)

    def test_toko_url_make_template(self):
        response = Client().get('/toko/')
        self.assertTemplateUsed(response, 'toko.html')

    def test_buat_toko_url_make_template(self):
        response = Client().get('/toko/tambah/')
        self.assertTemplateUsed(response, 'tambahToko.html')

    def test_toko_fungsi(self):
        found = resolve('/toko/')
        self.assertEqual(found.func, toko)

    def test_toko_fungsi(self):
        found = resolve('/toko/tambah/')
        self.assertEqual(found.func, tambahToko)