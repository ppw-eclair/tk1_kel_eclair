from django.db import models

# Create your models here.
class Toko(models.Model):
    foto=models.ImageField(upload_to='fotoko/', default="")
    nama=models.CharField(max_length=100)
    nomor=models.CharField(max_length=14)
    email=models.CharField(max_length=100)
    username=models.CharField(max_length=100)
    password=models.CharField(max_length=100)

    def __str__(self):
        return self.nama