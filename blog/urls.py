from django.urls import path
from . import views

app_name = "blog"
urlpatterns = [
    path('', views.blog_view, name="blog_view"),
    path('buat', views.buat_blog, name="form_blog"),
    path('delete/<id>', views.blog_delete, name="hapus"),
]