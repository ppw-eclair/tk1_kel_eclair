from django.test import TestCase, Client
from blog.views import buat_blog, blog_view, blog_delete
from blog.models import BlogModels
from django.urls import resolve
from django.core.files.base import File
from django.http import HttpRequest
from django.core.files.uploadedfile import SimpleUploadedFile
# from PIL import Image

class BlogUnitTest(TestCase):
    
    def test_blog_url_is_exist(self):
        response = Client().get('/blog/')
        self.assertEqual(response.status_code,200)
    
    def test_buat_url_exist(self):
        response = Client().get('/blog/buat')
        self.assertEqual(response.status_code,200)
    
    def test_blog_using_view_blog_template(self):
        response = Client().get('/blog/')
        self.assertTemplateUsed(response, 'view_blog.html')
    
    def test_buat_blog_using_buat_blog_template(self):
        response = Client().get('/blog/buat')
        self.assertTemplateUsed(response, 'buat_blog.html')
    
    def test_model_str(self):
        test_model = BlogModels(judul = "Aku", deskripsi = "test aja", id=1)
        test_model.save()
        test_model = BlogModels.objects.get(judul="Aku")
        stringnya = str(test_model)
        self.assertEqual("Aku", stringnya)
    
    def test_views_delete(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        test_delete = BlogModels(judul = "judul", deskripsi = "deskripsi", foto = uploaded, id=1)
        test_delete.save()
        test_delete.delete()
        self.assertEqual(BlogModels.objects.all().count(),0)





        # test_views = BlogModels(
        #     {'judul' : "teri",
        #         'foto': uploaded,
        #         'deksripsi' : "teri enak",
        #     }
        # )            
        # test_views.save()
        # self.assertEqual(len(test_views),1)
        # self.assertEqual(test_views[0].judul, "teri")
        # self.assertEqual(test_views[0].foto, 'foto_blog/small.gif')
        # self.assertEqual(test_views[0].deskripsi, 'teri enak')
        

        
