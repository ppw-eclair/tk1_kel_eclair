from django.forms import ModelForm
from .models import BlogModels

class BlogModelsForms(ModelForm):
    class Meta:
        model = BlogModels
        fields = [
            'judul',
            'deskripsi',
            'foto',
        ]
