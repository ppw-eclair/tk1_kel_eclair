from django.shortcuts import render
from .forms import BlogModelsForms
from .models import BlogModels
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
def buat_blog(request):
    
    if(request.method == 'POST'):
        form = BlogModelsForms(request.POST, request.FILES)
        if(form.is_valid()):
            form.save()
            return redirect ('blog:form_blog')
    else:
        form = BlogModelsForms()
        context = {
            'form':form,
            'all_response':BlogModels.objects.all(),
        }
        return render(request,"buat_blog.html",context)   

def blog_view(request):
    data = BlogModels.objects.all()
    return render(request, 'view_blog.html', {'data': data})

def blog_delete(request, id):
    blog_hapus = BlogModels.objects.get(id=id)
    blog_hapus.delete()
    return redirect('blog:blog_view')

