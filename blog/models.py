from django.db import models

class BlogModels(models.Model):

    judul = models.CharField(max_length=120)
    deskripsi = models.CharField(max_length=500)
    foto = models.ImageField(upload_to='foto_blog/')

    def __str__(self):
        return self.judul

# Create your models here.
