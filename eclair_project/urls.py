"""eclair_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from home_app.views import *
from beli_ikan.views import *
from django.conf import settings
from django.conf.urls.static import static 
from toko.views import *
from faq.views import *
from blog.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home_app.urls')),
    path('toko/', include('toko.urls')),
    path('beli/', include('beli_ikan.urls')),
    path('faq/', include('faq.urls')),
    path('blog/', include('blog.urls') )

]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)